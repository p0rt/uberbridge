#!/usr/bin/env python3
# -*- coding: utf-8 -*-


import config
import signal
from importlib import reload
import logging
logging.basicConfig(filename=config.logfile, filemode='a', level=config.log_level, format='%(asctime)s - %(levelname)s - %(message)s')
logger = logging.getLogger(__name__)

# define a function to catch signals and reload the config - reloading is done with USR1
def reloadConfig(signalNumber, frame):
    try:
        reload(config)
        logger.info("[config] reloaded config")
    except Exception as e:
        logger.warning("[config] couldn't reload config!")
        logger.info(e)
signal.signal(signal.SIGUSR1, reloadConfig)




# ============== XMPP =====

#import

class XMPPClient():
    def __init__(self):
        pass


    def send_text(self, body, forward_dest):
        pass


    def send_file(self, url, forward_dest):
        pass


    def send_image(self, url, forward_dest):
        pass


    def send_video(self, url, forward_dest):
        pass



# ============== FACEBOOK =====

from fbchat import Client
from fbchat.models import *
from wget import download
from os import unlink

class FBClient(Client):
    def onMessage(self, author_id, message_object, thread_id, thread_type, ts, metadata, msg, **kwargs):
        """
        :param author_id: The ID of the author
        :param message_object: The message (As a `Message` object)
        :param thread_id: Thread ID that the message was sent to. See :ref:`intro_threads`
        :param thread_type: Type of thread that the message was sent to. See :ref:`intro_threads`
        :param ts: The timestamp of the message
        :param metadata: Extra metadata about the message
        :param msg: A full set of the data recieved
        :type message_object: models.Message
        :type thread_type: models.ThreadType
        """

        if author_id == self.uid:
            logger.info("[FB] Read self sent message.")
            logger.debug("[FB] ===")
            logger.debug(metadata, msg)
            logger.debug("========\n")
            return

        self.markAsDelivered(author_id, thread_id)
        self.markAsRead(author_id)
        self.markAsSeen()

        author_name = self.fetchUserInfo(author_id)[author_id].name

        try:
            telegram_forward_dest = config.FB.telegram_destination[thread_id]
        except KeyError:
            telegram_forward_dest = False
            logger.warning("[FB] Telegram bridge destination unknown.")

        try:
            xmpp_forward_dest = config.FB.xmpp_destination[thread_id]
        except KeyError:
            xmpp_forward_dest = False
            logger.warning("[FB] XMPP bridge destination unknown.")

        if not message_object.text:
            logger.info("[FB] Skipping empty message.")
        elif message_object.text[0] == '!':
            logger.info("[FB] Skipping silenced message.")
        else:
            forward_text = author_name + ":\n" + message_object.text
            if telegram_forward_dest:
                tg.send_text(forward_text, telegram_forward_dest)
            if xmpp_forward_dest:
                xmpp.send_text(forward_text, xmpp_forward_dest)

        for attachment in message_object.attachments:
            if type(attachment) is FileAttachment:
                logger.info("[FB] Received a file.")
                if telegram_forward_dest:
                    tg.send_text(author_name + " sent a file...", telegram_forward_dest)
                    tg.send_text(attachment.url, telegram_forward_dest)
                if xmpp_forward_dest:
                    xmpp.send_text(author_name + " sent a file...", xmpp_forward_dest)
                    xmpp.send_text(attachment.url, xmpp_forward_dest)
            elif type(attachment) is ShareAttachment:
                #TODO: ShareAttachments are important, but not yet implemented
                logger.info("[FB] Received a ShareAttachment, which is not yet supported.")
                if telegram_forward_dest:
                    tg.send_text(author_name + " shared something I can't yet share with you :(", telegram_forward_dest)
                if xmpp_forward_dest:
                    xmpp.send_text(author_name + " shared something I can't yet share with you :(", xmpp_forward_dest)
            elif type(attachment) is ImageAttachment:
                #TODO: send full-sized images as files
                logger.info("[FB] Received an image.")
                if telegram_forward_dest:
                    tg.send_image(attachment.large_preview_url, telegram_forward_dest)
                    tg.send_text(author_name + " sent an image...", telegram_forward_dest)
                if xmpp_forward_dest:
                    xmpp.send_image(attachment.large_preview_url, xmpp_forward_dest)
                    xmpp.send_text(author_name + " sent an image...", xmpp_forward_dest)
            elif type(attachment) is VideoAttachment:
                logger.info("[FB] Received a video.")
                if telegram_forward_dest:
                    tg.send_video(attachment.preview_url, telegram_forward_dest)
                    tg.send_text(author_name + " sent a video...", telegram_forward_dest)
                if xmpp_forward_dest:
                    xmpp.send_video(attachment.preview_url, xmpp_forward_dest)
                    xmpp.send_text(author_name + " sent a video...", xmpp_forward_dest)
            elif type(attachment) is AudioAttachment:
                logger.info("[FB] Received an audio file.")
                if telegram_forward_dest:
                    tg.send_file(attachment.url, telegram_forward_dest)
                    tg.send_text(author_name + " sent an audio file...", telegram_forward_dest)
                if xmpp_forward_dest:
                    xmpp.send_video(attachment.url, xmpp_forward_dest)
                    xmpp.send_text(author_name + " sent an audio file...", xmpp_forward_dest)
            elif type(attachment) is LocationAttachment:
                logger.info("[FB] Received location.")
                attachment_forward_text = author_name + " sent location:\n"
                attachment_forward_text += "geo:" + attachment.latitude + "," + attachment.longitude + "\n"
                if attachment.address:
                    attachment_forward_text += "address: " + attachment.address + "\n"
                attachment_forward_text += "map link: " + attachment.url
                if telegram_forward_dest:
                    tg.send_text(attachment_forward_text, telegram_forward_dest)
                if xmpp_forward_dest:
                    xmpp.send_text(attachment_forward_text, xmpp_forward_dest)
            else:
                logger.error("[FB] Received some kind of unknown attachment!")

        if message_object.sticker:
            logger.info("[FB] Received a sticker.")
            if telegram_forward_dest:
                tg.send_image(message_object.sticker.url, telegram_forward_dest)
                tg.send_text(author_name + " sent a sticker...", telegram_forward_dest)
            if xmpp_forward_dest:
                xmpp.send_image(message_object.sticker.url, xmpp_forward_dest)
                xmpp.send_text(author_name + " sent a sticker...", xmpp_forward_dest)

        logger.info("[FB] {} from {}, {}. Thread type: {}".format(message_object, author_id, thread_id, thread_type.name))
        logger.debug("[FB] ===")
        logger.debug(metadata, msg)
        logger.debug("========\n")


    def onPollCreated(self, poll, author_id, thread_id, thread_type, ts, metadata, msg, **kwargs):
        """
        :param poll: Created poll
        :param author_id: The ID of the person who created the poll
        :param thread_id: Thread ID that the action was sent to. See :ref:`intro_threads`
        :param thread_type: Type of thread that the action was sent to. See :ref:`intro_threads`
        :param ts: A timestamp of the action
        :param metadata: Extra metadata about the action
        :param msg: A full set of the data recieved
        :type poll: models.Poll
        :type thread_type: models.ThreadType
        """

        if author_id == self.uid:
            logger.info("[FB] Read self sent message.")
            logger.debug("[FB] ===")
            logger.debug(metadata, msg)
            logger.debug("========\n")
            return

        self.markAsDelivered(author_id, thread_id)
        self.markAsRead(author_id)
        self.markAsSeen()

        author_name = self.fetchUserInfo(author_id)[author_id].name

        try:
            telegram_forward_dest = config.FB.telegram_destination[thread_id]
        except KeyError:
            telegram_forward_dest = False
            logger.warning("[FB] Telegram bridge destination unknown.")

        try:
            xmpp_forward_dest = config.FB.xmpp_destination[thread_id]
        except KeyError:
            xmpp_forward_dest = False
            logger.warning("[FB] XMPP bridge destination unknown.")

        forward_text = author_name + " created a poll:\n"
        forward_text += "Title: " + poll.title + "\n"
        forward_text += "Options:"

        for option in self.fetchPollOptions(poll.uid):
            forward_text += "\n - " + option.text

        if telegram_forward_dest:
            tg.send_text(forward_text, telegram_forward_dest)
        if xmpp_forward_dest:
            xmpp.send_text(forward_text, xmpp_forward_dest)

        logger.info("[FB] {} created poll {}, {}. Thread type: {}".format(author_id, poll, thread_id, thread_type.name))
        logger.debug("[FB] ===")
        logger.debug(metadata, msg)
        logger.debug("========\n")


    def wave_it(self, forward_dest):
        logger.info("[FB] Waving at %s", forward_dest)
        self.wave(wave_first=True, thread_id=forward_dest, thread_type=ThreadType.GROUP)


    def send_text(self, body, forward_dest):
        logger.info("[FB] Sending a text message to %s", forward_dest)
        self.send(Message(body), thread_id=forward_dest, thread_type=ThreadType.GROUP)


    def send_file(self, url, forward_dest):
        logger.info("[FB] Sending a remote file to %s", forward_dest)
        self.sendRemoteFiles([url], message=Message(text=''), thread_id=forward_dest, thread_type=ThreadType.GROUP)


    def send_image(self, url, forward_dest):
        logger.info("[FB] Downloading photo from %s", url)
        photo_filename = download(url)
        logger.info("[FB] Sending photo to %s", forward_dest)
        self.sendLocalImage(photo_filename, message=Message(text=''), thread_id=forward_dest, thread_type=ThreadType.GROUP)
        logger.info("[FB] Removing photo from %s", photo_filename)
        unlink(photo_filename)


    def send_video(self, url, forward_dest):
        self.send_file(url, forward_dest)



# ============== TELEGRAM =====

from telegram.ext import Updater, MessageHandler, Filters, CommandHandler

class TGClient():
    def __init__(self):
        self.updater = Updater(config.TG.bot_api_key)
        self.dp = self.updater.dispatcher

        self.dp.add_handler(MessageHandler(Filters.text, self.parse_text))
        self.dp.add_handler(MessageHandler(Filters.photo, self.parse_photo))
        self.dp.add_handler(MessageHandler(Filters.document | Filters.audio | Filters.video | Filters.voice, self.parse_document))
        self.dp.add_handler(MessageHandler(Filters.location, self.parse_location))
        self.dp.add_handler(MessageHandler(Filters.sticker, self.parse_sticker))

        self.dp.add_handler(CommandHandler("wave", self.wave_it))

        self.dp.add_error_handler(self.error)
        self.updater.start_polling()
        logger.info("[TG] Logged in. Started polling.")


    def destinations(self, update):
        try:
            facebook_forward_dest = config.TG.facebook_destination[update.effective_chat.id]
        except KeyError:
            facebook_forward_dest = False
            logger.warning("[TG] Facebook bridge destination unknown.")
        try:
            xmpp_forward_dest = config.TG.xmpp_destination[update.effective_chat.id]
        except KeyError:
            xmpp_forward_dest = False
            logger.warning("[TG] XMPP bridge destination unknown.")
        return (facebook_forward_dest, xmpp_forward_dest)


    def username(self, user):
        username = user.first_name
        if user.last_name is not None:
            username += " " + user.last_name
        return username


    def parse_text(self, bot, update):
        if update.message.text[0] == '!':
            logger.info("[TG] Skipping silented message.")
            return

        forward_body = self.username(update.message.from_user) + ":"

        if update.message.forward_from is not None:
            forward_body += "\n(FWD from " + self.username(update.message.forward_from) + ")"

        if update.message.reply_to_message:
            forward_body += "\n>>" + self.username(update.message.reply_to_message.from_user)
            for line in update.message.reply_to_message.text.split('\n'):
                forward_body += "\n>" + line

        forward_body += "\n"
        forward_body += update.message.text

        facebook_forward_dest, xmpp_forward_dest = self.destinations(update)

        if facebook_forward_dest:
            fb.send_text(forward_body, facebook_forward_dest)
        if xmpp_forward_dest:
            xmpp.send_text(forward_body, xmpp_forward_dest)

        logger.info("[TG] Sent a text message.")


    def parse_photo(self, bot, update):
        caption_body = self.username(update.message.from_user) + " sent an image..."
        if update.message.caption:
            caption_body += "\n" + update.message.caption

        image = update.message.photo[-1].get_file().file_path

        facebook_forward_dest, xmpp_forward_dest = self.destinations(update)

        if facebook_forward_dest:
            fb.send_text(caption_body, facebook_forward_dest)
            fb.send_image(image, facebook_forward_dest)
        if xmpp_forward_dest:
            xmpp.send_text(caption_body, xmpp_forward_dest)
            xmpp.send_image(image, xmpp_forward_dest)

        logger.info("[TG] Sending photo %s", image)


    def parse_document(self, bot, update):
        caption_body = self.username(update.message.from_user) + " sent a file..."
        if update.message.caption:
            caption_body += "\n" + update.message.caption

        document = update.message.effective_attachment.get_file()
        document = document.file_path

        facebook_forward_dest, xmpp_forward_dest = self.destinations(update)

        if facebook_forward_dest:
            fb.send_text(caption_body, facebook_forward_dest)
            fb.send_file(document, facebook_forward_dest)
        if xmpp_forward_dest:
            xmpp.send_text(caption_body, xmpp_forward_dest)
            xmpp.send_file(document, xmpp_forward_dest)

        logger.info("[TG] Sending a file %s", document)


    def parse_location(self, bot, update):
        forward_body = self.username(update.message.from_user) + " sent location:\n"
        if update.message.forward_from is not None:
            forward_body += "(FWD from " + self.username(update.message.forward_from) + ")\n"
        forward_body += "geo:" + str(update.message.location.latitude) + "," + str(update.message.location.longitude) + "\n"

        facebook_forward_dest, xmpp_forward_dest = self.destinations(update)

        if facebook_forward_dest:
            fb.send_text(forward_body, facebook_forward_dest)
        if xmpp_forward_dest:
            xmpp.send_text(forward_body, xmpp_forward_dest)


    def parse_sticker(self, bot, update):
        caption_body = self.username(update.message.from_user) + " sent a sticker..."
        image = update.message.sticker.get_file().file_path

        if update.message.forward_from is not None:
            caption_body += "\n(FWD from " + self.username(update.message.forward_from) + ")"

        facebook_forward_dest, xmpp_forward_dest = self.destinations(update)

        if facebook_forward_dest and config.TG.facebook_forward_stickers:
            fb.send_text(caption_body, facebook_forward_dest)
            # apparently .webp is not an image on facebook, so it wouldn't send with send_image
            fb.send_file(image, facebook_forward_dest)
        if xmpp_forward_dest:
            xmpp.send_text(caption_body, xmpp_forward_dest)
            xmpp.send_image(image, xmpp_forward_dest)

        logger.info("[TG] Sending a sticker %s", image)


    def wave_it(self, bot, update):
        facebook_forward_dest, xmpp_forward_dest = self.destinations(update)
        if facebook_forward_dest:
            fb.send_wave(facebook_forward_dest)


    def error(self, bot, update, error):
        logger.error('[TG] "%s" caused error "%s"!', update, error)


    def send_text(self, body, forward_dest):
        logger.info("[TG] Sending a text message to %s", forward_dest)
        self.updater.bot.send_message(forward_dest, body)


    def send_file(self, url, forward_dest):
        logger.info("[TG] Sending a file to %s", forward_dest)
        self.updater.bot.send_document(forward_dest, url)


    def send_image(self, url, forward_dest):
        logger.info("[TG] Sending an image to %s", forward_dest)
        self.updater.bot.send_photo(forward_dest, url)


    def send_video(self, url, forward_dest):
        logger.info("[TG] Sending a video to %s", forward_dest)
        self.updater.bot.send_video(forward_dest, url)



# ============== BRIDGE =====

class Main():
    def __init__(self):
        self.facebook_side = FBClient(config.FB.login, config.FB.password, logging_level=config.log_level)
        self.telegram_side = TGClient()
        self.xmpp_side = XMPPClient()
        logger.info("All set.")

    def listen(self):
        self.facebook_side.listen()


main = Main()
fb = main.facebook_side
tg = main.telegram_side
xmpp = main.xmpp_side

if __name__ == "__main__":
    main.listen()

