# UberBridge

facebook-telegram-XMPP puppeted bridge

Dependencies:
 - [fbchat](https://github.com/carpedm20/fbchat)
   - requires python(3)-lxml
 - [python-telegram-bot](https://github.com/python-telegram-bot/python-telegram-bot)

XMPP is not *yet* supported. WIP
